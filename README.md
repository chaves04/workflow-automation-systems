# Workflow Automation Systems



## CONFIGURAÇÕES DOS ARQUIVOS .BPM


Para abrir/editar os arquivos (.bpm) será necessário a instalação da ferramenta 
"bizagi modeler" que pode ser obtida gratuitamente através do link 
https://www.bizagi.com/pt/produtos/bpm-suite/modeler 




## DESCRIÇÃO DA FERRAMENTA BIZAGI MODELER

A ferramenta “Bizagi Modeler” se encarrega de desenhar o diagrama de fluxo do processo, com as atividades (tarefas), pontos de tomada de decisão, eventos e subprocessos. Os principais elementos que o bizagi oferece para a modelagem do processo são:
	    

### Pool:
É o container onde o processo será modelado, ele carrega o nome do processo.
	    
### Lane:
Usado para diferenciar os atores do processo (por exemplo usuário, analista, executor, etc.)

### Milestone:

Usado para diferenciar as fases do processo (por exemplo abertura de solicitação, publicação, execução, aceitação do cliente, etc.)
	    
### Tasks: Atividades do processo, essas atividades podem ser de 7 tipos: 
-Usuário (atividade realizada por uma pessoa, interno ao processo, portanto o fluxo só continua após a interferência humana na aplicação); 
-Serviço (atividade realizada pela máquina, sem interferência humana);
-Roteiro (atividade que executa um roteiro (script) relacionado ao banco de dados); 
-Manual (atividade realizada por uma pessoa, externo ao processo); 
-Envio de e-mail (atividade utilizada para envio de e-mails); 
-Recebimento de e-mail (atividade que recebe um e-mail para que o fluxo prossiga);
-Regra de negócio (atividade que executa uma regra de negócio).

### Gateways: 

São representados por losangos, sendo os pontos de tomada de decisão ou pontos de convergência, podendo ser um gateway convergente (converge vários caminhos em um único) ou divergente (diverge um caminho em vários). Em qualquer tipo, os gateways podem ser de 5 tipos: 

Paralelo [Representado por um simbolo (+) no centro]

*Convergente: fluxo só segue quando todos os caminhos conseguirem chegar até o gateway;

*Divergente: aciona dois caminhos ao mesmo tempo.	

-Exclusivo baseado em dados [Representado por um simbolo (x) no centro]

*Convergente: une rotas alternativas em um só caminho. O fluxo segue após a primeira rota atingir o gateway;

*Divergente: representa uma decisão exclusiva, ou seja, somente um dos caminhos de saída é seguido dependendo de alguma decisão.

-Exclusivo baseado em eventos [Representado por um simbolo em forma de pentagono com um circulo ao redor, centralizado]

*Apenas pode ser divergente: os caminhos alternativos são ativados e, após um desses caminhos ser selecionado, os demais são desabilitados, não se baseia em dados do processo, mas em eventos.

-Inclusivo [Representado por um simbolo (o) no centro]

*Convergente: sincroniza em um único caminho várias rotas que foram habilitadas usando um gateway inclusiva divergente; 

*Divergente: cada fluxo de sequência de saída possui uma condição independente, baseada em dados do processo, com mais de uma condição podendo ser verdadeira.

-Complexo [Representado por um simbolo (*) no centro]

*Convergente: permite modelar comportamentos complexos de sincronização, onde por exemplo, duas de três condições tem que ser atingidas para que o processo continue; 

*Divergente: controla um ponto de decisão complexo dentro do processo.
	    
### Events: 

Ações que se tratam de acontecimentos externos e que não dependem da pessoa a quem são atribuídas. Pode ser de 9 tipos: 


-Intermediário [Representado por um círculo com linhas duplas] 

*Tipo catch: aguarda a ocorrência de um fato para que o processo continua;

*Tipo throw: geram a ocorrência de um fato e o processo continua.

-Intermediário de temporização [Representado por um círculo com um relógio no centro] 

*Representa uma pausa por algum tempo determinado no fluxo do processo.

-Temporizador anexo aos limites [Representado em uma task, por um círculo com um relógio no centro, junto à task]

*Representa uma ação que será executada quando não seja cumprido o tempo definido para o desenvolvimento da atividade.

-Fim [Representado por um círculo vermelho (com linhas duplas caso seja terminal)]

*Sem especificar: indica o final de uma rota do processo; 

*Terminal: indica o final do processo, se for usado em algum subprocesso ou mesmo no processo pai, finalizará o fluxo do processo pai.

-Intermediário de erro [Representado por um círculo com um raio no centro]

*Indica que o fluxo será finalizado com um alerta de erro.

-Erro anexo aos limites [Representado em uma task, por um círculo com um raio no centro, junto à task]

*Representa uma ação que será executada quando um erro ocorrer em uma task.

-Evento de mensagem [Representado por um círculo com linhas duplas e uma carta no centro]

*Permite a comunicação entre processos através de mensagens, podendo ser usado apenas uma vez por processo.

-Evento de sinal [Representado por um círculo com linhas duplas e um triangulo no centro]

*Emissão: envia um sinal e continua com o fluxo do processo; 

*Recepção: espera um sinal para prosseguir no fluxo do processo.

-Início [Representado por um círculo verde]

*indica que o processo se inicia naquele ponto.

