São considerados Eventos de Interesse à Saúde: doenças transmissíveis, não transmissíveis e agravos em determinada localidade.
 
São considerados Eventos Relevantes: Eventos de saúde pública (ESP): situação que pode constituir potencial ameaça à saúde pública, como a ocorrência de surto ou epidemia, doença ou agravo de causa desconhecida, alteração no padrão clínico epidemiológico das doenças conhecidas, considerando o potencial de disseminação, a magnitude, a gravidade, a severidade, a transcendência e a vulnerabilidade, bem como epizootias ou agravos decorrentes de desastres ou acidentes

Os eventos considerados como ESP são doenças, agravos e eventos constantes do Anexo I da Portaria nº 1.271, de 06 de Junho de 2014.
